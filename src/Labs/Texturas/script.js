import * as THREE from "https://threejsfundamentals.org/threejs/resources/threejs/r132/build/three.module.js";
import { OrbitControls } from "https://threejsfundamentals.org/threejs/resources/threejs/r132/examples/jsm/controls/OrbitControls.js";

import Assets from "../../Assets.js";

const WIDTH = window.innerWidth - 10;
const HEIGHT = window.innerHeight - 10;

const scene = new THREE.Scene();

// const VISIBLE = false;
const VISIBLE = true;

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setClearColor("#f0f0f0");
renderer.setSize(WIDTH, HEIGHT);
document.body.appendChild(renderer.domElement);

// Camera
const fov = 75;
const aspect = 2;  // the canvas default
const near = 0.1;
const far = 1000;
const position = { x: 0, y: 0, z: 20 };
const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
const controls = new OrbitControls(camera, renderer.domElement);
camera.position.z = position.z;

// Objetos
const objects = [];
const loader = new THREE.TextureLoader();

{// Cube1
	const geometry = new THREE.BoxGeometry(5, 5, 5);
	const material = new THREE.MeshBasicMaterial({ map: loader.load(Assets.Images.Ninica.Ninica1) });
	const cube = new THREE.Mesh(geometry, material);
	cube.position.x = -15;
	cube.visible = VISIBLE;
	objects.push(cube);
}

{// Cube2
	const geometry = new THREE.BoxGeometry(1, 3, 5);
	const materials = [
		new THREE.MeshBasicMaterial({ map: loader.load('https://picsum.photos/200/300') }), // Direita
		new THREE.MeshBasicMaterial({ map: loader.load('https://picsum.photos/200') }), // Esquerda
		new THREE.MeshBasicMaterial({ map: loader.load('https://picsum.photos/200/300?grayscale') }),  // Cima
		new THREE.MeshBasicMaterial({ map: loader.load('https://picsum.photos/200/300/?blur') }), // Baixo
		new THREE.MeshBasicMaterial({ map: loader.load('https://picsum.photos/200/300.jpg') }), // Frente
		new THREE.MeshBasicMaterial({ map: loader.load('https://picsum.photos/200/300') }), // Trás
	];
	// const materialsColors = [
	// 	new THREE.MeshBasicMaterial({ color: "#000000", }), // Direita
	// 	new THREE.MeshBasicMaterial({ color: "#ff0000", }), // Esquerda
	// 	new THREE.MeshBasicMaterial({ color: "#00ff00", }), // Cima
	// 	new THREE.MeshBasicMaterial({ color: "#00ffff", }), // Baixo
	// 	new THREE.MeshBasicMaterial({ map: "#0000ff", }), // Frente
	// 	new THREE.MeshBasicMaterial({ color: "#ffffff", }), // Trás
	// ];
	const cube = new THREE.Mesh(geometry, materials);
	cube.position.set(0, 7.5, 0);
	cube.visible = VISIBLE;
	objects.push(cube);
}

{// Cone
	const geometry = new THREE.ConeGeometry(3, 5, 15);
	const material = new THREE.MeshBasicMaterial({ map: loader.load(Assets.Images.Ninica.Ninica2) });
	const cone = new THREE.Mesh(geometry, material);
	cone.position.x = 15;
	cone.visible = VISIBLE;
	objects.push(cone);
}

{// Sphere
	const geometry = new THREE.SphereGeometry(5, 32, 32);
	const material = new THREE.MeshBasicMaterial({ map: loader.load(Assets.Sprites.Dino.Idle[3]) });
	const sphere = new THREE.Mesh(geometry, material);
	sphere.position.set(0, -7.5, -20);
	sphere.visible = VISIBLE;
	objects.push(sphere);
}

// Manipulação de texturas
// Girar, Rpetição, Deslocamento, "Girando"
// https://threejsfundamentals.org/threejs/lessons/threejs-textures.html#uvmanipulation

{// Cube3 mover textura
	const geometry = new THREE.BoxGeometry(2.5, 2.5, 2.5);
	const texture = loader.load(Assets.Sprites.Dino.Idle[3]);
	texture.offset.set(0.5, 0.5);
	const material = new THREE.MeshBasicMaterial({ map: texture });
	const cube = new THREE.Mesh(geometry, material);
	cube.position.set(0, -7, 0);
	cube.visible = VISIBLE;
	scene.add(cube);
}

{// Textura apenas em um lado
	const geometry = new THREE.BoxGeometry(2.5, 2.5, 2.5);
	const texture = loader.load(Assets.Sprites.Dino.Idle[3]);

	const materials = [
		new THREE.MeshBasicMaterial({ color: "#000000", }), // Direita
		new THREE.MeshBasicMaterial({ color: "#ff0000", }), // Esquerda
		new THREE.MeshBasicMaterial({ color: "#00ff00", }), // Cima
		new THREE.MeshBasicMaterial({ color: "#00ffff", }), // Baixo
		new THREE.MeshBasicMaterial({ map: texture, }), // Frente
		new THREE.MeshBasicMaterial({ color: "#ffffff", }), // Trás
	];
	const cube = new THREE.Mesh(geometry, materials);
	cube.position.set(-10, 0, -10);
	cube.visible = VISIBLE;
	scene.add(cube);
}

{ // Sphere textura
	const texture = loader.load(Assets.Texture.Floor.StoneFloorDiffuse);

	const geometry = new THREE.SphereGeometry(2, 32, 32);
	const material = new THREE.MeshBasicMaterial({ map: texture });
	const sphere = new THREE.Mesh(geometry, material);
	sphere.position.set(-7, -7, 5);
	sphere.visible = VISIBLE;
	scene.add(sphere);
}

{ // Sphere Olho
	const texture = loader.load(Assets.Texture.Olho.Olho);
	texture.repeat.set(1, 1);

	const geometry = new THREE.SphereGeometry(2, 32, 32);
	const material = new THREE.MeshBasicMaterial({ map: texture });
	const sphere = new THREE.Mesh(geometry, material);
	sphere.position.set(7, -7, 5);
	sphere.visible = VISIBLE;
	scene.add(sphere);
}
