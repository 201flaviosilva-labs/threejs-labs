import * as THREE from "https://threejsfundamentals.org/threejs/resources/threejs/r132/build/three.module.js";
import { OrbitControls } from "https://threejsfundamentals.org/threejs/resources/threejs/r132/examples/jsm/controls/OrbitControls.js";

// UI
let image = null;
const ModalDiv = document.getElementById("Modal");
const inputFile = document.getElementById("inputFile");
// --
const boxGeometric = document.getElementById("boxGeometric");
const ballGeometric = document.getElementById("ballGeometric");
const rectangleGeometric = document.getElementById("rectangleGeometric");
const circleGeometric = document.getElementById("circleGeometric");
const coneGeometric = document.getElementById("coneGeometric");
const cylinderGeometric = document.getElementById("cylinderGeometric");
// --
document.getElementById("btnOK").addEventListener("click", () => {
	image = inputFile.files[0];
	if (!image) {
		alert("Tens de adicionar uma imagem!");
		return;
	}
	ModalDiv.style.display = "none";
	createNewObject();
});

// Three
const WIDTH = window.innerWidth - 10;
const HEIGHT = window.innerHeight - 10;

const scene = new THREE.Scene();

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setClearColor("#f0f0f0");
renderer.setSize(WIDTH, HEIGHT);
document.body.appendChild(renderer.domElement);

// Camera
const fov = 75;
const aspect = 2;  // the canvas default
const near = 0.1;
const far = 1000;
const position = { x: 0, y: 0, z: 20 };
const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
const controls = new OrbitControls(camera, renderer.domElement);
camera.position.z = position.z;

// Objetos
const loader = new THREE.TextureLoader();


animate();
function animate() {
	requestAnimationFrame(animate);
	renderer.render(scene, camera);
}


function createNewObject() {
	let geometry = null;
	if (boxGeometric.checked) geometry = new THREE.BoxGeometry(4, 4, 4);
	else if (ballGeometric.checked) geometry = new THREE.SphereGeometry(2, 32, 32);
	else if (rectangleGeometric.checked) geometry = new THREE.BoxGeometry(2, 4, 8);
	else if (circleGeometric.checked) geometry = new THREE.CircleGeometry(7, 32);
	else if (coneGeometric.checked) geometry = new THREE.ConeGeometry(5, 10, 32);
	else if (cylinderGeometric.checked) geometry = new THREE.CylinderGeometry(4, 4, 8, 12);

	if (!geometry || !image) return;
	const texture = loader.load(URL.createObjectURL(image));
	const material = new THREE.MeshBasicMaterial({ map: texture });
	const object = new THREE.Mesh(geometry, material);
	scene.add(object);
}
