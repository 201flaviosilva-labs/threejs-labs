import * as THREE from "https://threejsfundamentals.org/threejs/resources/threejs/r132/build/three.module.js";

const WIDTH = window.innerWidth;
const HEIGHT = window.innerHeight;

const scene = new THREE.Scene();

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setClearColor("#e5e5e5");
renderer.setSize(WIDTH, HEIGHT);
document.body.appendChild(renderer.domElement);

// Camera
// const camera = new THREE.PerspectiveCamera(75, WIDTH / HEIGHT, 0.1, 1000);
const camera = new THREE.PerspectiveCamera(70, WIDTH / HEIGHT);
camera.position.z = 20;
scene.add(camera);
window.addEventListener("resize", () => {
	renderer.setSize(WIDTH, HEIGHT);
	camera.aspect = WIDTH / HEIGHT;
	camera.updateProjectionMatrix();
});

// Esfera
const geometryS = new THREE.SphereGeometry(1, 10, 10);
const materialS = new THREE.MeshPhongMaterial({ color: 0xff0000 }); // Afetado pela Luz (Phong)
const sphere = new THREE.Mesh(geometryS, materialS);
sphere.position.set(3, -2, 5);
scene.add(sphere);

// Cubo
const geometryC = new THREE.BoxGeometry(1, 1, 3);
const materialC = new THREE.MeshBasicMaterial({ color: 0x00ff00 }); // Não Afetado pela Luz (Basic)
const cube = new THREE.Mesh(geometryC, materialC);
cube.position.set(1, 5, 1);
cube.scale.set(1, 2, 1);
scene.add(cube);

// Anel
const geometryA = new THREE.TorusGeometry(3, 1, 6, 10);
const materialA = new THREE.MeshPhongMaterial({ color: 0xffff00 }); // Phong
const torus = new THREE.Mesh(geometryA, materialA);
scene.add(torus);

// Dodecaedro (Dado com muitas faces)
const geometryD = new THREE.DodecahedronGeometry(1);
const materialD = new THREE.MeshLambertMaterial({ color: 0xff00ff });  // Afetado pela Luz ((Lambert) Parecido ao Phong mas menos detalhado)
const dodecahedron = new THREE.Mesh(geometryD, materialD);
scene.add(dodecahedron);

// Cilindro
const geometryCil = new THREE.CylinderGeometry(1, 1, 2);
const materialCil = new THREE.MeshPhongMaterial({ color: 0x00ffff }); // Phong
const cylinder = new THREE.Mesh(geometryCil, materialCil);
cylinder.position.x = -5;
scene.add(cylinder);

// Luz
const light = new THREE.PointLight(0xffffff, 1, 100);
light.position.set(0, 0, 20);
scene.add(light);

let x = 0;
let y = 0;
let z = 0;
let ballSpeed = 0.1;
let cubeSpeed = { x: 0.1, y: 0.1 };
let ringSpeed = 0.1;
let diceSpeed = 0.1;
let cilSpeed = 0.01;
function animate() {
	requestAnimationFrame(animate);
	x += 0.01;
	y += 0.01;
	z += 0.01;
	cube.rotation.set(x, y, z);
	sphere.rotation.set(x, y, z);
	torus.rotation.set(x, y, z);
	dodecahedron.rotation.set(x, y, z);
	cylinder.rotation.set(x, y, z);

	mover();

	renderer.render(scene, camera);
}
animate();

function mover() {
	// Quadrado
	if (cube.position.x > 15 || cube.position.x < -15) cubeSpeed.x = cubeSpeed.x * -1;
	cube.position.x += cubeSpeed.x;

	if (cube.position.y > 12 || cube.position.y < -12) cubeSpeed.y = cubeSpeed.y * -1;
	cube.position.y += cubeSpeed.y;

	// Outros
	if (sphere.position.y > 10 || sphere.position.y < -10) ballSpeed = ballSpeed * -1;
	sphere.position.y += ballSpeed;

	if (torus.position.x > 20 || torus.position.x < -20) ringSpeed = ringSpeed * -1;
	torus.position.x += ringSpeed;

	if (dodecahedron.position.z > 18.5 || dodecahedron.position.z < -20) diceSpeed = diceSpeed * -1;
	dodecahedron.position.z += diceSpeed;

	if (cylinder.scale.y > 18.5 || cylinder.scale.y < 1) cilSpeed = cilSpeed * -1;
	cylinder.scale.y += cilSpeed;
}
