import * as THREE from "https://threejsfundamentals.org/threejs/resources/threejs/r132/build/three.module.js";

const WIDTH = window.innerWidth;
const HEIGHT = window.innerHeight;

const scene = new THREE.Scene();

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setClearColor("#cccccc");
renderer.setSize(WIDTH, HEIGHT);
document.body.appendChild(renderer.domElement);

// Camera
const fov = 75;
const aspect = 2;  // the canvas default
const near = 0.1;
const far = 1000;
const position = { x: 0, y: 0, z: 10 };
const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
camera.position.set(0, 50, 0);
camera.up.set(0, 0, 1);
camera.lookAt(0, 0, 0);

// Objetos
const objects = [];

// Sistema Solar
const solarSystem = new THREE.Object3D();
scene.add(solarSystem);
objects.push(solarSystem);

// Sol
const radius = 1;
const widthSegments = 10;
const heightSegments = 30;
const sphereGeometry = new THREE.SphereBufferGeometry(radius, widthSegments, heightSegments);
const sunMaterial = new THREE.MeshPhongMaterial({ emissive: 0xFFFF00 });
const sunMesh = new THREE.Mesh(sphereGeometry, sunMaterial);
sunMesh.scale.set(5, 5, 5);  // make the sun large
objects.push(sunMesh);
solarSystem.add(sunMesh);


// Orbita da Terra
const earthOrbit = new THREE.Object3D();
earthOrbit.position.x = 10;
solarSystem.add(earthOrbit);
objects.push(earthOrbit);

//Terra
const earthMaterial = new THREE.MeshPhongMaterial({ color: 0x2233FF, emissive: 0x112244 });
const earthMesh = new THREE.Mesh(sphereGeometry, earthMaterial);
earthOrbit.add(earthMesh);
objects.push(earthMesh);

// Orbita da Lua
const moonOrbit = new THREE.Object3D();
moonOrbit.position.x = 2;
earthOrbit.add(moonOrbit);

// Lua
const moonMaterial = new THREE.MeshPhongMaterial({ color: 0x888888, emissive: 0x222222 });
const moonMesh = new THREE.Mesh(sphereGeometry, moonMaterial);
moonMesh.scale.set(.5, .5, .5);
moonOrbit.add(moonMesh);
objects.push(moonMesh);

// adiciona um AxesHelper a cada nó
objects.forEach((node) => {
	const axes = new THREE.AxesHelper();
	axes.material.depthTest = false;
	axes.renderOrder = 1;
	node.add(axes);
});

// Luz
const color = 0xFFFFFF;
const intensity = 3;
const light = new THREE.PointLight(color, intensity);
// light.position.set(0, 0, position.z);
scene.add(light);

animate();
function animate(time) {
	time *= 0.001;
	const canvas = renderer.domElement;
	camera.aspect = canvas.clientWidth / canvas.clientHeight;
	camera.updateProjectionMatrix();

	objects.forEach((obj) => {
		obj.rotation.y = time;
	});

	requestAnimationFrame(animate);
	renderer.render(scene, camera);
}
