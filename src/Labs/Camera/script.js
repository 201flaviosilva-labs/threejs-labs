import * as THREE from "https://threejsfundamentals.org/threejs/resources/threejs/r132/build/three.module.js";
import { OrbitControls } from "https://threejsfundamentals.org/threejs/resources/threejs/r132/examples/jsm/controls/OrbitControls.js";
import { GUI } from 'https://threejsfundamentals.org/threejs/../3rdparty/dat.gui.module.js';

import { MinMaxGUIHelper } from "./MinMaxGUIHelper.js";

// https://threejsfundamentals.org/threejs/lessons/threejs-cameras.html

const WIDTH = window.innerWidth - 10;
const HEIGHT = window.innerHeight - 10;

const scene = new THREE.Scene();

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setClearColor("#f0f0f0");
renderer.setSize(WIDTH, HEIGHT);
document.body.appendChild(renderer.domElement);

// Camera
const fov = 45;
const aspect = 2;  // the canvas default
const near = 0.1;
const far = 100;
const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
camera.position.set(0, 10, 20);
function updateCamera() {
	camera.updateProjectionMatrix();
}

// Helper
const gui = new GUI();
gui.add(camera, 'fov', 1, 180).onChange(updateCamera);
const minMaxGUIHelper = new MinMaxGUIHelper(camera, 'near', 'far', 0.1);
gui.add(minMaxGUIHelper, 'min', 0.1, 50, 0.1).name('near').onChange(updateCamera);
gui.add(minMaxGUIHelper, 'max', 0.1, 200, 0.1).name('far').onChange(updateCamera);

// Controls
const controls = new OrbitControls(camera, renderer.domElement);
controls.target.set(0, 5, 0);
controls.update();

// Objetos
{
	const planeSize = 40;

	const loader = new THREE.TextureLoader();
	const texture = loader.load('https://threejsfundamentals.org/threejs/resources/images/checker.png');
	texture.wrapS = THREE.RepeatWrapping;
	texture.wrapT = THREE.RepeatWrapping;
	texture.magFilter = THREE.NearestFilter;
	const repeats = planeSize / 2;
	texture.repeat.set(repeats, repeats);

	const planeGeo = new THREE.PlaneBufferGeometry(planeSize, planeSize);
	const planeMat = new THREE.MeshPhongMaterial({
		map: texture,
		side: THREE.DoubleSide,
	});
	const mesh = new THREE.Mesh(planeGeo, planeMat);
	mesh.rotation.x = Math.PI * -.5;
	scene.add(mesh);
}
{
	const cubeSize = 4;
	const cubeGeo = new THREE.BoxBufferGeometry(cubeSize, cubeSize, cubeSize);
	const cubeMat = new THREE.MeshPhongMaterial({ color: '#8AC' });
	const mesh = new THREE.Mesh(cubeGeo, cubeMat);
	mesh.position.set(cubeSize + 1, cubeSize / 2, 0);
	scene.add(mesh);
}
let z = 5;
for (let i = 0; i < 10; i++) {
	{
		const sphereRadius = 3;
		const sphereWidthDivisions = 32;
		const sphereHeightDivisions = 16;
		const sphereGeo = new THREE.SphereBufferGeometry(sphereRadius, sphereWidthDivisions, sphereHeightDivisions);
		const sphereMat = new THREE.MeshPhongMaterial({ color: '#CA8' });
		const mesh = new THREE.Mesh(sphereGeo, sphereMat);
		mesh.position.set(-sphereRadius - 1, sphereRadius + 2, z);
		scene.add(mesh);
		z += -(sphereRadius * 3);
	}
}


// Luz
{
	const color = 0xFFFFFF;
	const intensity = 1;
	const light = new THREE.DirectionalLight(color, intensity);
	light.position.set(0, 10, 0);
	light.target.position.set(-5, 0, 0);
	scene.add(light);
	scene.add(light.target);
}

// Update
animate();
function animate() {
	// if (resizeRendererToDisplaySize(renderer)) {
	// 	const canvas = renderer.domElement;
	// 	camera.aspect = WIDTH / canvas.clientHeight;
	// 	camera.updateProjectionMatrix();
	// }
	renderer.render(scene, camera);
	requestAnimationFrame(animate);
}
function resizeRendererToDisplaySize(renderer) {
	const canvas = renderer.domElement;
	const width = WIDTH;
	const height = canvas.clientHeight;
	const needResize = canvas.width !== width || canvas.height !== height;
	if (needResize) {
		renderer.setSize(width, height, false);
	}
	return needResize;
}
