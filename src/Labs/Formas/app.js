import * as THREE from "https://threejsfundamentals.org/threejs/resources/threejs/r132/build/three.module.js";

import { randomColor } from "../../util.js";

const WIDTH = window.innerWidth;
const HEIGHT = window.innerHeight;

const scene = new THREE.Scene();

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setClearColor("#cccccc");
renderer.setSize(WIDTH, HEIGHT);
document.body.appendChild(renderer.domElement);

// Camera
const fov = 75;
const aspect = 2;  // the canvas default
const camera = new THREE.PerspectiveCamera(fov, aspect);
camera.position.z = 20;
scene.add(camera);

// Objetos
let objetos = [];

// Cubo
const geometry1 = new THREE.BoxGeometry(1, 1, 1);
const material1 = new THREE.MeshPhongMaterial({ color: randomColor() });
objetos.push(new THREE.Mesh(geometry1, material1));

const widthSegmentsCubo = 5;
const heightSegmentsCubo = 10;
const depthSegmentsCubo = 9;
const geometry2 = new THREE.BoxBufferGeometry(1, 3, 1, widthSegmentsCubo, heightSegmentsCubo, depthSegmentsCubo);
const material2 = new THREE.MeshBasicMaterial({ color: randomColor() });
objetos.push(new THREE.Mesh(geometry2, material2));

// Circulo
const radiusCirculos = 2; // tamanho
const segmentsCirculos = 10; // numero de "partições", quantas mais, mas redondo fica

const geometry3 = new THREE.CircleBufferGeometry(radiusCirculos, segmentsCirculos);
const material3 = new THREE.MeshPhongMaterial({ color: randomColor() });
objetos.push(new THREE.Mesh(geometry3, material3));

const thetaStartCirculos = Math.PI * 0; // Onde começa o Circulo
const thetaLengthCirculos = Math.PI * 1.40; // Não sei
const geometry4 = new THREE.CircleBufferGeometry(3, 50, thetaStartCirculos, thetaLengthCirculos);
const material4 = new THREE.MeshBasicMaterial({ color: randomColor() });
objetos.push(new THREE.Mesh(geometry4, material4));

// Cone
const radiusCone = 2; // Tamnho de fundo
const heightCone = 5; // Altura
const radialSegmentsCone = 11; // numero de "partições", quantas mais, mas redondo fica
const geometry5 = new THREE.ConeBufferGeometry(radiusCone, heightCone, radialSegmentsCone);
const material5 = new THREE.MeshPhongMaterial({ color: randomColor() });
objetos.push(new THREE.Mesh(geometry5, material5));

const heightSegmentsCone = 2;
const openEndedCone = true;
const thetaStartCone = Math.PI * 0.1;
const thetaLengthCone = Math.PI * 1.5;
const geometry6 = new THREE.ConeBufferGeometry(radiusCone, heightCone, 6, heightSegmentsCone, openEndedCone, thetaStartCone, thetaLengthCone);
const material6 = new THREE.MeshBasicMaterial({ color: randomColor() });
objetos.push(new THREE.Mesh(geometry6, material6));

// Cilindro
const radiusTopCilindro = 2;
const radiusBottomCilindro = 3;
const heightCilindro = 3;
const radialSegmentsCilindro = 4;
const geometry7 = new THREE.CylinderBufferGeometry(radiusTopCilindro, radiusBottomCilindro, heightCilindro, radialSegmentsCilindro);
const material7 = new THREE.MeshPhongMaterial({ color: randomColor() });
objetos.push(new THREE.Mesh(geometry7, material7));

const heightSegmentsCilindro = 2;
const openEndedCilindro = false;
const thetaStartCilindro = Math.PI * 0.25;
const thetaLengthCilindro = Math.PI * 1.5;
const geometry8 = new THREE.CylinderBufferGeometry(2, 2, 4, 20, heightSegmentsCilindro, openEndedCilindro, thetaStartCilindro, thetaLengthCilindro);
const material8 = new THREE.MeshBasicMaterial({ color: randomColor() });
objetos.push(new THREE.Mesh(geometry8, material8));

// https://threejsfundamentals.org/threejs/lessons/threejs-primitives.html
// Para muitos outros objetos e formas

let position = { x: -18, y: 13 };
objetos.map((o, i) => {
	o.position.x = position.x;
	o.position.y = position.y;
	position.x += 7;
	scene.add(o);
	if (i % 6 == 0) {
		position.y -= 5;
		position.x = -18;
	}
});

// Luz
const color = 0xFFFFFF;
const intensity = 1;
const light = new THREE.DirectionalLight(color, intensity);
light.position.set(-1, 2, 4);
scene.add(light);

let rotate = 0;
animate();
function animate() {
	// Responcivo
	const canvas = renderer.domElement;
	camera.aspect = canvas.clientWidth / canvas.clientHeight;
	camera.updateProjectionMatrix();

	rotate += 0.01;
	objetos.map(o => o.rotation.set(rotate, rotate, rotate));
	requestAnimationFrame(animate);
	renderer.render(scene, camera);
}
