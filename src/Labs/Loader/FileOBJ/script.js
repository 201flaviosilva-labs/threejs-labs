import * as THREE from "https://threejsfundamentals.org/threejs/resources/threejs/r132/build/three.module.js";
import { OrbitControls } from "https://threejsfundamentals.org/threejs/resources/threejs/r132/examples/jsm/controls/OrbitControls.js";
import { MTLLoader } from "https://threejsfundamentals.org/threejs/resources/threejs/r132/examples/jsm/loaders/MTLLoader.js";
import { OBJLoader } from "https://threejsfundamentals.org/threejs/resources/threejs/r132/examples/jsm/loaders/OBJLoader.js";

import Assets, { assetsPath } from "../../../Assets.js";

const WIDTH = window.innerWidth - 10;
const HEIGHT = window.innerHeight - 10;

const scene = new THREE.Scene();

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setClearColor("#f0f0f0");
renderer.setSize(WIDTH, HEIGHT);
document.body.appendChild(renderer.domElement);

// Camera
const fov = 45;
const aspect = WIDTH / HEIGHT;
const near = 0.1;
const far = 1000;
const position = { x: 0, y: 50, z: 100 };
const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
camera.position.set(position.x, position.y, position.z);
// Responcivo
window.addEventListener("resize", () => {
	renderer.setSize(WIDTH, HEIGHT);
	camera.aspect = WIDTH / HEIGHT;
	camera.updateProjectionMatrix();
});

// Controller
const controls = new OrbitControls(camera, renderer.domElement);
controls.target.set(0, 0, 0);

// Objetos
const loader = new THREE.TextureLoader();

// textura Chão
const floor = loader.load(Assets.Texture.Floor.StoneFloorDiffuse);

const planeSize = 100;

floor.wrapS = THREE.RepeatWrapping;
floor.wrapT = THREE.RepeatWrapping;
floor.magFilter = THREE.NearestFilter;
const repeats = planeSize / 2;
floor.repeat.set(repeats, repeats);

// Chão
const planeGeo = new THREE.PlaneBufferGeometry(planeSize, planeSize);
const planeMat = new THREE.MeshPhongMaterial({ map: floor, side: THREE.DoubleSide });
const mesh1 = new THREE.Mesh(planeGeo, planeMat);
mesh1.rotation.x = Math.PI * -.5;
scene.add(mesh1);

{
	// Carro Low Poly
	// Textura
	const loaderMTL = new MTLLoader();
	// loaderMTL.setTexturePath(assetsPath + "3D/CarLowPoly/");
	loaderMTL.setPath(assetsPath + "/3D/CarLowPoly/");
	loaderMTL.load("Car.mtl", (material) => {
		material.preload();

		// Objeto
		const loaderOBJ = new OBJLoader();
		loaderOBJ.setMaterials(material);
		loaderOBJ.setPath(assetsPath + "/3D/CarLowPoly/");
		loaderOBJ.load("Car.obj", (object) => {
			scene.add(object);
		});
	});
}

{
	// Carro Porsche
	// Textura
	const loaderMTL = new MTLLoader();
	loaderMTL.setPath(assetsPath + "3D/CarPorsche/");
	loaderMTL.load("Car.mtl", (material) => {
		material.preload();

		// Objeto
		const loaderOBJ = new OBJLoader();
		loaderOBJ.setMaterials(material);
		loaderOBJ.setPath(assetsPath + "3D/CarPorsche/");
		loaderOBJ.load("Car.obj", (object) => {
			object.position.set(13, 0, 15);
			object.rotation.y = 1.57;
			scene.add(object);
		});
	});
}

// Luz
// AmbientLight
const luzAmbiente = new THREE.AmbientLight(0xFFFFFF, 1);
scene.add(luzAmbiente);

// PointLight
const luzPoint = new THREE.PointLight(0xFFFFFF, 1);
luzPoint.position.set(0, 50, 0);
const luzHelper = new THREE.PointLightHelper(luzPoint);
scene.add(luzPoint);
scene.add(luzHelper);

// Update
update();
function update() {
	renderer.render(scene, camera);
	requestAnimationFrame(update);
}
