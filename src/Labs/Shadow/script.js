import * as THREE from "https://threejsfundamentals.org/threejs/resources/threejs/r132/build/three.module.js";
import { OrbitControls } from "https://threejsfundamentals.org/threejs/resources/threejs/r132/examples/jsm/controls/OrbitControls.js";

const WIDTH = window.innerWidth - 10;
const HEIGHT = window.innerHeight - 10;

const assets = "../../../Assets/";

const scene = new THREE.Scene();

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setClearColor("#f0f0f0");
renderer.setSize(WIDTH, HEIGHT);
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFSoftShadowMap;
document.body.appendChild(renderer.domElement);

// Camera
const fov = 75;
const aspect = 2;  // the canvas default
const near = 0.1;
const far = 1000;
const position = { x: 0, y: 10, z: 20 };
const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
camera.position.x = position.x;
camera.position.y = position.y;
camera.position.z = position.z;
// const cameraHelper = new THREE.CameraHelper(camera);
// scene.add(cameraHelper);


// Controller
const controls = new OrbitControls(camera, renderer.domElement);
controls.target.set(0, 0, 0);
controls.update();

{
	// Chão
	const planeSize = 100;

	const planeGeo = new THREE.PlaneBufferGeometry(planeSize, planeSize);
	const planeMat = new THREE.MeshPhongMaterial({ color: 0x00ff00 });
	const plano = new THREE.Mesh(planeGeo, planeMat);
	plano.rotation.x = Math.PI * -.5;
	plano.castShadow = true;
	plano.receiveShadow = true;
	scene.add(plano);
}

{
	// Cubo
	const geometry = new THREE.BoxGeometry(5, 5, 5);
	const material = new THREE.MeshPhongMaterial({ color: 0xff00ff });
	const cube = new THREE.Mesh(geometry, material);
	cube.position.x = 0;
	cube.position.y = 2.5;
	cube.castShadow = true;
	cube.receiveShadow = true; //default
	scene.add(cube);
}

{
	// Cubo
	const geometry = new THREE.BoxGeometry(2, 10, 2);
	const material = new THREE.MeshPhongMaterial({ color: 0xff0000 });
	const cube = new THREE.Mesh(geometry, material);
	cube.position.x = 0;
	cube.position.y = 2.5;
	cube.position.z = 7.5;
	cube.castShadow = true;
	cube.receiveShadow = true; //default
	scene.add(cube);
}

{
	// Luz Ambiente
	const color = 0xffffff;
	const intensity = 0.05;
	const luzAmbiente = new THREE.AmbientLight(color, intensity);
	scene.add(luzAmbiente);
}

{
	// Luz Point
	const color = 0xffffff;
	const intensity = 1;
	const light = new THREE.PointLight(color, intensity);
	light.position.x = 0;
	light.position.y = 10;
	light.position.z = 10;
	light.castShadow = true;


	light.shadow.mapSize.width = 512; // default
	light.shadow.mapSize.height = 512; // default
	light.shadow.camera.near = 0.5; // default
	light.shadow.camera.far = 500; // default

	const helper = new THREE.PointLightHelper(light);
	const shadowHelp = new THREE.CameraHelper(light.shadow.camera);
	scene.add(light);
	scene.add(helper);
	scene.add(shadowHelp);
}

{
	// Luz Point
	const color = 0xffffff;
	const intensity = 1;
	const light = new THREE.PointLight(color, intensity);
	light.position.x = 0;
	light.position.y = 10;
	light.position.z = -10;
	light.castShadow = true;

	const helper = new THREE.PointLightHelper(light);
	const shadowHelp = new THREE.CameraHelper(light.shadow.camera);
	scene.add(light);
	scene.add(helper);
	scene.add(shadowHelp);
}

animate();
function animate() {
	controls.update();
	renderer.render(scene, camera);
	requestAnimationFrame(animate);
}
