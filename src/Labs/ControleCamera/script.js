import * as THREE from "https://threejsfundamentals.org/threejs/resources/threejs/r132/build/three.module.js";
import { OrbitControls } from "https://threejsfundamentals.org/threejs/resources/threejs/r132/examples/jsm/controls/OrbitControls.js";

const WIDTH = window.innerWidth - 10;
const HEIGHT = window.innerHeight - 10;

const scene = new THREE.Scene();

const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setClearColor("#ffffff");
renderer.setSize(WIDTH, HEIGHT);
document.body.appendChild(renderer.domElement);

const fov = 45;
const aspect = 2;  // the canvas default
const near = 0.1;
const far = 100;
const camera = new THREE.PerspectiveCamera(70, WIDTH / HEIGHT);
// const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
const controls = new OrbitControls(camera, renderer.domElement);
camera.position.z = 20;
scene.add(camera);
window.addEventListener("resize", () => {
	renderer.setSize(WIDTH, HEIGHT);
	camera.aspect = WIDTH / HEIGHT;
	camera.updateProjectionMatrix();
});
camera.position.set(0, 20, 100);
controls.update();

// Luz
const light = new THREE.PointLight(0xffff00, 1, 100);
light.position.set(0, 10, 20);
scene.add(light);

const geometia = new THREE.BoxGeometry(10, 10, 10);
const material = new THREE.MeshPhongMaterial({ color: 0xff00ff });
const cube = new THREE.Mesh(geometia, material);
scene.add(cube);

animate();
function animate() {
	requestAnimationFrame(animate);
	controls.update();
	renderer.render(scene, camera);
}
