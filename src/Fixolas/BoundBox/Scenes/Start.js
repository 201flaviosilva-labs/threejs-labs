import * as THREE from "https://threejsfundamentals.org/threejs/resources/threejs/r132/build/three.module.js";
import { OrbitControls } from "https://threejsfundamentals.org/threejs/resources/threejs/r132/examples/jsm/controls/OrbitControls.js";
import Stats from "https://cdnjs.cloudflare.com/ajax/libs/stats.js/r17/Stats.min.js";
import { randomNumber, randomColor, randomColorRGB, randomColorRGBA } from "../../../util.js";

export default class Game {
	constructor() {
		this.init();
		this.create();
		this.update();
	}

	init() {
		this.WIDTH = window.innerWidth - 10;
		this.HEIGHT = window.innerHeight - 10;

		this.loader = new THREE.TextureLoader();
	}

	create() {
		// Scene
		this.scene = new THREE.Scene();
		this.status();
		this.render();
		this.camera();
		this.controller();
		this.responsive();
		this.createBox();
		this.ambientLight();
	}

	status() {
		this.stats = new Stats();
		this.stats.showPanel(0);
		document.body.appendChild(this.stats.dom);
	}

	render() {
		// Render
		this.renderer = new THREE.WebGLRenderer({ antialias: true, });
		this.renderer.setClearColor(randomColorRGBA());
		this.renderer.setSize(this.WIDTH, this.HEIGHT);
		document.body.appendChild(this.renderer.domElement);
	}

	controller() {
		// Controller
		this.controls = new OrbitControls(this.camera, this.renderer.domElement);
		this.controls.target.set(0, 0, 0);
		this.controls.update();
	}

	camera() {
		// Camera
		const fov = 45;
		const aspect = this.WIDTH / this.HEIGHT;
		const near = 0.1;
		const far = 1000;
		const position = { x: 0, y: 250, z: 250 };
		this.camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
		this.camera.position.set(position.x, position.y, position.z);
	}

	responsive() {
		// Responsivo
		window.addEventListener("resize", () => {
			this.renderer.setSize(this.WIDTH, this.HEIGHT);
			this.camera.aspect = this.WIDTH / this.HEIGHT;
			this.camera.updateProjectionMatrix();
		});
	}

	createBox() {
		// MaxBox
		const maxSize = randomNumber(50, 250);
		for (let i = 0; i < maxSize; i += randomNumber(1, 10)) {
			const geometryPlayer = new THREE.BoxGeometry(i, i, i);
			const materialPlayer = new THREE.MeshPhongMaterial({ color: randomColor(), opacity: 0.1, transparent: true, });
			const bounds = new THREE.Mesh(geometryPlayer, materialPlayer);
			bounds.position.set(0, 0, 0);
			this.scene.add(bounds);
		}
	}

	ambientLight() {
		// AmbientLight
		const luzAmbiente = new THREE.AmbientLight(0xFFFFFF, 1);
		this.scene.add(luzAmbiente);
	}

	update() {
		// Update
		this.stats.begin();
		this.renderer.render(this.scene, this.camera);
		this.controls.update();
		this.stats.end();
		requestAnimationFrame(this.update.bind(this));
	}
}
