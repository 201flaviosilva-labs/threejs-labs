import * as THREE from "https://threejsfundamentals.org/threejs/resources/threejs/r132/build/three.module.js";
import { OrbitControls } from "https://threejsfundamentals.org/threejs/resources/threejs/r132/examples/jsm/controls/OrbitControls.js";

import materials from "./materiais.js";
import { randomNumber } from "../../util.js";

const WIDTH = window.innerWidth - 10;
const HEIGHT = window.innerHeight - 10;

// Scene
const scene = new THREE.Scene();

// Render
const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setClearColor("#f0f0f0");
renderer.setSize(WIDTH, HEIGHT);
document.body.appendChild(renderer.domElement);

// Camera
const camera = new THREE.PerspectiveCamera(55, WIDTH / HEIGHT, 45, 30000);
camera.position.set(-900, -200, -900);

// controls
const controls = new OrbitControls(camera, renderer.domElement);
controls.minDistance = 1;
controls.maxDistance = 7500;
controls.rotateSpeed *= -1;

// SkyBox
const keys = Object.keys(materials);
const material = materials[`${keys[randomNumber(0, keys.length - 1)]}`];

material.forEach(m => m.side = THREE.BackSide);

const skyboxGeo = new THREE.BoxGeometry(10000, 10000, 10000);
const skybox = new THREE.Mesh(skyboxGeo, material);
scene.add(skybox);

function update() {
	renderer.render(scene, camera);
	controls.update();
	requestAnimationFrame(update);
}
update();
