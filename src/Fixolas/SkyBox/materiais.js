import * as THREE from "https://threejsfundamentals.org/threejs/resources/threejs/r132/build/three.module.js";

import Assets, { assetsPath } from "../../Assets.js";
const assets = assetsPath + "/Image/SkyBox/";


const loader = new THREE.TextureLoader();
const materials = {
	arid: [
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins/arid_ft.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins/arid_bk.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins/arid_up.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins/arid_dn.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins/arid_rt.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins/arid_lf.jpg") })
	],
	arid2: [
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (2)/arid2_ft.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (2)/arid2_bk.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (2)/arid2_up.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (2)/arid2_dn.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (2)/arid2_rt.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (2)/arid2_lf.jpg") })
	],
	barren: [
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (3)/barren_ft.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (3)/barren_bk.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (3)/barren_up.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (3)/barren_dn.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (3)/barren_rt.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (3)/barren_lf.jpg") })
	],
	battery: [
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (4)/battery_ft.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (4)/battery_bk.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (4)/battery_up.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (4)/battery_dn.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (4)/battery_rt.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (4)/battery_lf.jpg") })
	],
	bay: [
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (5)/bay_ft.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (5)/bay_bk.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (5)/bay_up.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (5)/bay_dn.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (5)/bay_rt.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (5)/bay_lf.jpg") })
	],
	blizzard: [
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (6)/blizzard_ft.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (6)/blizzard_bk.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (6)/blizzard_up.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (6)/blizzard_dn.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (6)/blizzard_rt.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (6)/blizzard_lf.jpg") })
	],
	cocoa: [
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (7)/cocoa_ft.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (7)/cocoa_bk.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (7)/cocoa_up.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (7)/cocoa_dn.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (7)/cocoa_rt.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (7)/cocoa_lf.jpg") })
	],
	desertdawn: [
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (8)/desertdawn_ft.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (8)/desertdawn_bk.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (8)/desertdawn_up.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (8)/desertdawn_dn.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (8)/desertdawn_rt.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (8)/desertdawn_lf.jpg") })
	],
	barren: [
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (9)/divine_ft.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (9)/divine_bk.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (9)/divine_up.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (9)/divine_dn.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (9)/divine_rt.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (9)/divine_lf.jpg") })
	],
	dusk: [
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (10)/dusk_ft.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (10)/dusk_bk.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (10)/dusk_up.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (10)/dusk_dn.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (10)/dusk_rt.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (10)/dusk_lf.jpg") })
	],
	dust: [
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (11)/dust_ft.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (11)/dust_bk.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (11)/dust_up.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (11)/dust_dn.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (11)/dust_rt.jpg") }),
		new THREE.MeshBasicMaterial({ map: loader.load(assets + "penguins (11)/dust_lf.jpg") })
	],
};

export default materials;
