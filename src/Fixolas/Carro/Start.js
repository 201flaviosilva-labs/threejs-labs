import * as THREE from "https://threejsfundamentals.org/threejs/resources/threejs/r132/build/three.module.js";
import { OrbitControls } from "https://threejsfundamentals.org/threejs/resources/threejs/r132/examples/jsm/controls/OrbitControls.js";
import { MTLLoader } from "https://threejsfundamentals.org/threejs/resources/threejs/r132/examples/jsm/loaders/MTLLoader.js";
import { OBJLoader } from "https://threejsfundamentals.org/threejs/resources/threejs/r132/examples/jsm/loaders/OBJLoader.js";
import Stats from "https://cdnjs.cloudflare.com/ajax/libs/stats.js/r17/Stats.min.js";
import Assets, { assetsPath } from "../../Assets.js";


let tecla = null;
export default class Game {
	constructor() {
		this.init();
		this.create();
		this.update();
	}

	init() {
		this.WIDTH = window.innerWidth - 10;
		this.HEIGHT = window.innerHeight - 10;

		this.loader = new THREE.TextureLoader();
	}

	create() {
		// Scene
		this.scene = new THREE.Scene();
		this.status();
		this.render();
		this.camera();
		this.responsive();
		this.controller();
		this.objects();
		this.light();
	}

	status() {
		this.stats = new Stats();
		this.stats.showPanel(0);
		document.body.appendChild(this.stats.dom);
	}

	render() {
		// Render
		this.renderer = new THREE.WebGLRenderer({ antialias: true });
		this.renderer.setClearColor("#f0f0f0");
		this.renderer.setSize(this.WIDTH, this.HEIGHT);
		document.body.appendChild(this.renderer.domElement);
	}

	controller() {
		// Controller
		this.controls = new OrbitControls(this.camera, this.renderer.domElement);
		this.controls.target.set(0, 0, 0);
		this.controls.update();
	}

	camera() {
		// Camera
		const fov = 45;
		const aspect = this.WIDTH / this.HEIGHT;
		const near = 0.1;
		const far = 1000;
		const position = { x: 0, y: 50, z: 100 };
		this.camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
		this.camera.position.set(position.x, position.y, position.z);
	}

	responsive() {
		// Responcivo
		window.addEventListener("resize", () => {
			this.renderer.setSize(this.WIDTH, this.HEIGHT);
			this.camera.aspect = this.WIDTH / this.HEIGHT;
			this.camera.updateProjectionMatrix();
		});
	}

	objects() {
		// Objetos
		this.chao();
		this.paredes();
		this.criarPlayer();
	}

	chao() {
		// Textura Chão
		const floor = this.loader.load(Assets.Texture.Floor.StoneFloorDiffuse);

		const planeSize = 100;

		floor.wrapS = THREE.RepeatWrapping;
		floor.wrapT = THREE.RepeatWrapping;
		floor.magFilter = THREE.NearestFilter;
		const repeats = planeSize / 2;
		floor.repeat.set(repeats, repeats);

		// Chão
		const planeGeo = new THREE.PlaneBufferGeometry(planeSize, planeSize);
		const planeMat = new THREE.MeshPhongMaterial({ map: floor, side: THREE.DoubleSide });
		const floorMesh = new THREE.Mesh(planeGeo, planeMat);
		floorMesh.rotation.x = Math.PI * -.5;
		this.scene.add(floorMesh);
	}

	paredes() {
		// Paredes
		// Parede Direita
		const geometryPD = new THREE.BoxGeometry(1, 10, 100);
		const materialPD = new THREE.MeshBasicMaterial({ color: 0xff0000 });
		const paredeD = new THREE.Mesh(geometryPD, materialPD);
		paredeD.position.set(50, 5, 0);
		this.scene.add(paredeD);

		// Parede Esquerda
		const geometryPE = new THREE.BoxGeometry(1, 10, 100);
		const materialPE = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
		const paredeE = new THREE.Mesh(geometryPE, materialPE);
		paredeE.position.set(-50, 5, 0);
		this.scene.add(paredeE);

		// Parede Frente
		const geometryPF = new THREE.BoxGeometry(100, 10, 1);
		const materialPF = new THREE.MeshBasicMaterial({ color: 0x0000ff });
		const paredeF = new THREE.Mesh(geometryPF, materialPF);
		paredeF.position.set(0, 5, 50);
		this.scene.add(paredeF);

		// Parede Tras
		const geometryPT = new THREE.BoxGeometry(100, 10, 1);
		const materialPT = new THREE.MeshBasicMaterial({ color: 0xffff00 });
		const paredeT = new THREE.Mesh(geometryPT, materialPT);
		paredeT.position.set(0, 5, -50);
		this.scene.add(paredeT);
	}

	criarPlayer() {
		// Player
		// this.tecla = 0;
		this.player = null;
		tecla = null;
		// this.bola();
		this.carro();
		this.mudarDirecao();
	}

	bola() {
		const geometryPlayer = new THREE.SphereGeometry(1, 32, 32);
		const materialPlayer = new THREE.MeshPhongMaterial({ color: 0x00ffff });
		this.player = new THREE.Mesh(geometryPlayer, materialPlayer);
		this.player.position.set(0, 1, 0);
		this.scene.add(this.player);
	}

	carro() {
		const loaderMTL = new MTLLoader();
		loaderMTL.setPath(assetsPath + "/3D/CarLowPoly/");
		loaderMTL.load("Car.mtl", (material) => {
			material.preload();
			// Objeto
			const loaderOBJ = new OBJLoader();
			loaderOBJ.setMaterials(material);
			loaderOBJ.setPath(assetsPath + "/3D/CarLowPoly/");
			loaderOBJ.load("Car.obj", (object) => {
				this.player = object;
				this.scene.add(this.player);
			});
		});
	}

	mudarDirecao() {
		// Obter a tecla selecionada
		document.addEventListener("keydown", teclaDown, false);
		function teclaDown(event) {
			const key = event.code;
			// this.tecla = key;
			tecla = key;
		};

		document.addEventListener("keyup", teclaUp, false);
		function teclaUp() {
			// this.tecla = null;
			tecla = null;
		};
	}

	light() {
		// Luz
		// this.luzAmbiente();
		this.luzPonto();
	}

	luzAmbiente() {
		// AmbientLight
		const luzAmbiente = new THREE.AmbientLight(0xFFFFFF, 1);
		this.scene.add(luzAmbiente);
	}

	luzPonto() {
		// PointLight
		const luzPoint = new THREE.PointLight(0xFFFFFF, 1);
		luzPoint.position.set(0, 50, 0);
		const luzHelper = new THREE.PointLightHelper(luzPoint);
		this.scene.add(luzPoint);
		this.scene.add(luzHelper);
	}

	update() {
		// Update
		this.stats.begin();
		const game = this;
		this.renderer.render(this.scene, this.camera);
		this.controls.update();
		if (this.player) this.mover();
		// requestAnimationFrame(function () { game.update() });
		requestAnimationFrame(this.update.bind(this));
		this.stats.end();
	}

	mover() {
		// console.log(this.tecla);
		const speed = 1;
		const maxPosition = 50 - 4;

		// if ((this.tecla === "KeyD" || this.tecla === "ArrowRight") && this.player.position.x <= maxPosition) {
		// this.player.rotation.set(0, 0, 0);
		// 	this.player.rotateY(22.5 * Math.PI);
		// 	this.player.position.x += speed;

		// } else if ((this.tecla === "KeyA" || this.tecla === "ArrowLeft") && this.player.position.x >= -maxPosition) {
		// this.player.rotation.set(0, 0, 0);
		// 	this.player.rotateY(67.5 * Math.PI);
		// 	this.player.position.x -= speed;

		// } else if ((this.tecla === "KeyS" || this.tecla === "ArrowDown") && this.player.position.z <= maxPosition) {
		// this.player.rotation.set(0, 0, 0);
		// 	this.player.position.z += speed;

		// } else if ((this.tecla === "KeyW" || this.tecla === "ArrowUp") && this.player.position.z >= -maxPosition) {
		// this.player.rotation.set(0, 0, 0);
		// 	this.player.rotateY(45 * Math.PI);
		// 	this.player.position.z -= speed;

		// } else if (this.tecla === "Space") {// Reset
		// this.player.rotation.set(0, 0, 0);
		// 	this.player.position.set(0, 1, 0);
		// }

		if ((tecla === "KeyD" || tecla === "ArrowRight") && this.player.position.x <= maxPosition) {
			this.player.rotation.set(0, 0, 0);
			this.player.rotateY(22.5 * Math.PI);
			this.player.position.x += speed;

		} else if ((tecla === "KeyA" || tecla === "ArrowLeft") && this.player.position.x >= -maxPosition) {
			this.player.rotation.set(0, 0, 0);
			this.player.rotateY(67.5 * Math.PI);
			this.player.position.x -= speed;

		} else if ((tecla === "KeyS" || tecla === "ArrowDown") && this.player.position.z <= maxPosition) {
			this.player.rotation.set(0, 0, 0);
			this.player.position.z += speed;

		} else if ((tecla === "KeyW" || tecla === "ArrowUp") && this.player.position.z >= -maxPosition) {
			this.player.rotation.set(0, 0, 0);
			this.player.rotateY(45 * Math.PI);
			this.player.position.z -= speed;

		} else if (tecla === "Space") {// Reset
			this.player.rotation.set(0, 0, 0);
			this.player.position.set(0, 1, 0);
		}
	}
}
